const { client } = require('@xmpp/client');
const debug = require('@xmpp/debug');

const xmpp = client({
  service: 'xmpp://prosody:5222', // Replace 'prosody' with your server's hostname or IP
  domain: 'your-domain.com',       // Replace with your XMPP domain
  username: 'bot-username',        // Replace with your bot's username
  password: 'bot-password',        // Replace with your bot's password
});

// Uncomment the next line to enable debugging
// debug(xmpp, true);

xmpp.on('error', (err) => {
  console.error(err);
});

xmpp.on('offline', () => {
  console.log('Bot is offline');
});

xmpp.on('stanza', async (stanza) => {
  if (stanza.is('message')) {
    const body = stanza.getChildText('body');
    if (body) {
      console.log(`Incoming message: ${body}`);
      // Process message here (e.g., check for commands)
      if (body === 'ping') {
        sendMessage(stanza.attrs.from, 'pong');
      }
    }
  }
});

async function sendMessage(to, message) {
  const messageStanza = xml('message', { to, type: 'chat' },
    xml('body', {}, message)
  );
  await xmpp.send(messageStanza);
}

xmpp.on('online', async (address) => {
  console.log(`Bot is online as ${address}`);
  // Initial presence
  const presence = xml('presence');
  await xmpp.send(presence);
});

// Connect the bot
xmpp.start().catch(console.error);

